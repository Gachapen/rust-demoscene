extern crate ncurses;
extern crate rand;
extern crate png;

use std::string::String;
use std::char;
use std::thread;
use std::time::Duration;
use std::time::Instant;
use std::cmp::Ord;
use std::fs::File;

use ncurses as nc;
use rand::distributions::{IndependentSample, Range};

fn main() {
    nc::initscr();
    nc::curs_set(nc::CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    nc::start_color();

    //print_colors();
    run_matrix();

    nc::endwin();
}

fn run_matrix() {
    nc::nodelay(nc::stdscr, true);

    //let dark_green = (nc::COLORS - 1) as i16;
    //nc::init_color(dark_green, 365, 416, 0);

    //let outputs = vec!('0', '1');
    //let char_range = Range::new(0, outputs.len());

    let colors = vec!(64, 70);
    let color_range = Range::new(0, colors.len());

    for color in colors.iter() {
        nc::init_pair(*color as i16, *color as i16, nc::COLOR_BLACK);
    }

    let mut rng = rand::thread_rng();

    let (rows, cols) = get_win_size();

    //for i in 0..(cols * rows) {
    //    let row = i / cols;
    //    let col = i % cols;
    //    let output = outputs[char_range.ind_sample(&mut rng)];
    //    let color = colors[color_range.ind_sample(&mut rng)];

    //    nc::wmove(nc::stdscr, row, col);
    //    nc::attron(nc::COLOR_PAIR(color));
    //    nc::addch(output as u64);
    //}

    struct Image {
        width: usize,
        height: usize,
        data: Vec<u8>,
    };

    let image = {
        let decoder = png::Decoder::new(File::open("data/test.png").unwrap());
        let (info, mut reader) = decoder.read_info().unwrap();
        let mut img_data = vec![0; info.buffer_size()];
        reader.next_frame(&mut img_data).unwrap();

        if info.color_type != png::ColorType::Grayscale {
            unreachable!("Image needs to be grayscale.");
        }

        Image {
            width: info.width as usize,
            height: info.height as usize,
            data: img_data,
        }
    };

    //let image = {
    //    let (w, h) = (10, 10);

    //    Image {
    //        width: w,
    //        height: h,
    //        //data: (0..w*h).map(|_| {
    //        //    32u8
    //        //}).collect(),
    //        data: vec![
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //            16, 16, 16, 01, 01, 01, 01, 16, 16, 16,
    //            16, 16, 16, 01, 01, 01, 01, 16, 16, 16,
    //            16, 16, 16, 01, 01, 01, 01, 16, 16, 16,
    //            16, 16, 16, 01, 01, 01, 01, 16, 16, 16,
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //            16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    //        ],
    //    }
    //};

    let screen_image = {
        assert!(image.width <= cols as usize && image.height <= rows as usize);
        let mut screen_map = vec![1u8; cols as usize * rows as usize];
        for (i, strength) in image.data.iter().enumerate() {
            let row = i / image.width;
            let col = i % image.width;

            screen_map[row * cols as usize + col] = *strength;
        }

        screen_map
    };

    let (density_map, density_dist) = {
        let mut image_map: Vec<(usize, u8)> = (0..).zip(screen_image.iter().cloned()).collect();
        image_map.sort_by(|a, b| a.0.cmp(&b.0));

        let density_map = image_map.iter().map(|&(i, x)| (i, x as f32 / 255.0)).collect::<Vec<_>>();
        let density_sum = density_map.iter().fold(0f32, |sum, &(_, x)| sum + x);

        let mut density_dist = Vec::<(usize, f32)>::with_capacity(density_map.len());
        let mut acc = 0.0f32;
        for &(i, val) in &density_map {
            acc += val / density_sum;
            density_dist.push((i, acc));
        }

        (density_map, density_dist)
    };

    #[derive(Clone)]
    struct PixelState {
        col: i32,
        row: i32,
        time_alive: Duration,
    };

    impl PixelState {
        fn new(col: i32, row: i32) -> PixelState {
            PixelState {
                col: col,
                row: row,
                time_alive: Duration::new(0, 0)
            }
        }
    }

    let mut pixel_states = Vec::<PixelState>::with_capacity(cols as usize * rows as usize);

    let sleep_ns = 10000;
    let ttl = Duration::new(1, 0);
    let rand_range = Range::new(0.0, 1.0);
    //let greyscale_char: String = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ".chars().rev().collect();
    let greyscale_char = " .:-=+*#%@";

    let mut prev = Instant::now();

    loop {
        {
            let input = nc::getch();
            if input != nc::ERR && input != nc::KEY_RESIZE {
                break;
            }
        }

        let duration = {
            let now = Instant::now();
            let duration = now.duration_since(prev);
            prev = now;

            duration
        };

        {
            let mut pixel_index = 0;
            while pixel_index < pixel_states.len() {
                pixel_states[pixel_index].time_alive += duration;

                let pixel = pixel_states[pixel_index].clone();

                if pixel.time_alive > ttl {
                    nc::wmove(nc::stdscr, pixel.row, pixel.col);
                    nc::addch(' ' as u64);
                    pixel_states.swap_remove(pixel_index);
                } else {
                    pixel_index += 1;
                }
            }
        };

        let (rows, cols) = get_win_size();

        let pixel_index = {
            let rand = rand_range.ind_sample(&mut rng);

            let mut pos = 0;

            for &(i, r) in &density_dist {
                if rand <= r {
                    pos = i;
                    break;
                }
            }

            pos
        };

        let (row, col) = {
            (pixel_index as i32 / cols, pixel_index as i32 % cols)
        };

        pixel_states.push(PixelState::new(col, row));

        let output = {
            let density = density_map[pixel_index].1;
            let char_index = ((density * 0.999) * greyscale_char.len() as f32) as usize;
            greyscale_char.as_bytes()[char_index]
        };

        //let output = outputs[char_range.ind_sample(&mut rng)];
        let color = colors[color_range.ind_sample(&mut rng)];

        nc::wmove(nc::stdscr, row, col);
        nc::attron(nc::COLOR_PAIR(color));
        nc::addch(output as u64);

        thread::sleep(Duration::new(0, sleep_ns));
    }
}

fn sort<T: Ord + Clone>(vec: &Vec<T>) -> Vec<T> {
    let mut copy = vec.clone();
    copy.sort();
    copy
}

fn print_colors() {
    for c in 0..nc::COLORS {
        nc::init_pair(c as i16, c as i16, nc::COLOR_BLACK);
        nc::attron(nc::COLOR_PAIR(c as i16));
        nc::wmove(nc::stdscr, 0, c);
        nc::addch(digit_to_ch(c));
    }

    nc::getch();
}

fn get_win_size() -> (i32, i32) {
    let (mut rows, mut cols) = (0, 0);
    nc::getmaxyx(nc::stdscr, &mut rows, &mut cols);
    (rows, cols)
}

fn digit_to_ch(digit: i32) -> u64 {
    (digit + 48) as u64
}

fn number_to_str(number: i32) -> String {
    let num_digits = ((number as f32).log10() + 1.0) as i32;

    (0..num_digits).map(|i| {
        let digit = (number / 10i32.pow((num_digits - i - 1) as u32)) % 10;
        assert!(digit >= 0);

        if let Some(digit_char) = char::from_digit(digit as u32, 10) {
            return digit_char;
        } else {
            panic!("Could not convert digit {} to char", digit);
        }
    }).collect()
}
